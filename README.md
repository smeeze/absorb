# ABSORB

a simple C++ remote procedure calling library, using simple introspection
to autogenerate stubs and skeletons from C++ classes.
Where the C++ introspection cannot provide any more information, the library
writes simple header files for serializing/deserializing the arguments
and return values. This is done in the stub/skeleton that is generated.

The project borrows some ideas from CORBA, but does not claim to be anywhere
as complete as that.

Main platforms are Linux and Windows. Compiling the project on Windows will
sometimes fail due to features not available in the MSVC c++ compiler.

Under heavy development. API is not stable.

## requirements
* [boost](http://boost.org) sources. Minimal version is 1.66.0.
* C++ compiler that supports C++14
* cmake 3.11

## setup
create a user.cmake file in the root of your project. Add a
line pointing to the boost sources, like so:
```
set(BOOST_SOURCES /home/user/download/boost_1_66_0)
```

## build
build using standard cmake. i.e. create build directory and
from there, call cmake:
```
cmake -DCMAKE_BUILD_TYPE=Release /path/to/absorb && cmake --build .
```

## running
binaries are produced under the build root in cmake/project. There's an echo
client/server example. Run, in one terminal:
```
/path/to/absorb-build/cmake/project/test-echo-server
```
and in another terminal:
```
/path/to/absorb-build/cmake/project/test-echo-client ip-address-of-server
```
