#pragma once

// system
#include <vector>
// boost
#include <boost/type_index/ctti_type_index.hpp>
// absorb
#include <absorb/reflect/type.h>
#include <absorb/traits.h>

namespace absorb {
namespace reflect {
class function {
public:
  using arguments_type = std::vector<type>;

  std::string _hash;
  std::string _name;
  type _return_type;
  arguments_type _arguments;
  bool _is_const;

  /// Construct a function from a class method (non-const)
  template <typename Return, typename Class, typename... Args>
  function(const std::string &hash, const std::string &name,
           Return (Class::*)(Args...))
      : _hash(hash), _name(name) {
    _return_type._name =
        boost::typeindex::ctti_type_index::type_id<Return>().pretty_name(),
    _return_type._is_void = absorb::traits::is_void<Return>();

    absorb::traits::param_pack_helper<sizeof...(
        Args)>::template go<std::vector<type>, Args...>(_arguments);

    _is_const = false;
  }

  /// Construct a function from a class method (const)
  template <typename Return, typename Class, typename... Args>
  function(const std::string &hash, const std::string &name,
           Return (Class::*)(Args...) const)
      : _hash(hash), _name(name) {
    _return_type._name =
        boost::typeindex::ctti_type_index::type_id<Return>().pretty_name(),
    _return_type._is_void = absorb::traits::is_void<Return>();

    absorb::traits::param_pack_helper<sizeof...(
        Args)>::template go<std::vector<type>, Args...>(_arguments);
    _is_const = true;
  }
};
}
}
