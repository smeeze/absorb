#pragma once

// system
#include <string>
// boost
#include <boost/format.hpp>
// absorb
#include <absorb/reflect/function.h>
#include <absorb/reflect/namespace_.h>

namespace absorb {
namespace reflect {
namespace detail {
//
//
//
class class_ {
public:
  absorb::reflect::namespace_ _namespace;
  std::vector<absorb::reflect::function> _functions;

  class_(const absorb::reflect::namespace_ &n) : _namespace(n) {}

  auto get_name() const { return _namespace.back(); }
  auto get_full() const { return _namespace.full(); }

  template <typename Func> class_ &def(const std::string &name, Func &&f) {
    absorb::reflect::function cf(boost::str(boost::format("x%i") % (_x++)),
                                 name, std::forward<Func>(f));
    _functions.emplace_back(cf);
    return *this;
  }

private:
  int _x=0;
};
}
//
//
//
template <typename T> auto class_() {
  return detail::class_(namespace_(absorb::reflect::namespace_::make<T>()));
}
}
}
