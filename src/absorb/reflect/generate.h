#pragma once

// system
#include <ostream>
// absorb
#include <absorb/reflect/class_.h>

namespace absorb {
namespace reflect {
class generate {
public:
  static void skeleton(const std::string &, const absorb::reflect::detail::class_ &);
  static void skeleton(std::ostream &, const absorb::reflect::detail::class_ &);

  static void stub(const std::string &, const absorb::reflect::detail::class_ &);
  static void stub(std::ostream &, const absorb::reflect::detail::class_ &);
};
}
}
