#pragma once

// system
#include <string>
#include <vector>
// boost
#include <boost/type_index/ctti_type_index.hpp>

namespace absorb {
namespace reflect {
class namespace_ {
public:
  namespace_(const std::string &);

  template <typename T> static namespace_ make() {
    return namespace_(
        boost::typeindex::ctti_type_index::type_id<T>().pretty_name());
  }

  std::string full() const;
  std::string leading() const;
  const std::string &back() const { return _paths.back(); }

private:
  std::vector<std::string> _paths;
};
}
}
