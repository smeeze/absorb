#pragma once

// system
#include <iomanip>
#include <ostream>
#include <string>

namespace absorb {
namespace reflect {
  struct type {
    int         _index = -1;
    std::string _name;
    bool        _is_void = false;
    bool        _is_reference = false;
    bool        _is_const = false;
    bool        _is_pointer = false;
    bool        _is_shared_pointer = false;
  };
}
}

inline
std::ostream &
operator<<(std::ostream & out, const absorb::reflect::type & t) {
  out << "t[" << std::setw(3) << t._index
      << "] " << t._name
      ;
  if(t._is_void)      { out << "[void]"; }
  if(t._is_reference) { out << "[reference]"; }
  if(t._is_const)     { out << "[const]"; }

  return out;
}
