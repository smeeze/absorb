// smeeze views
#include <smeeze/views/split.h>
// absorb
#include <absorb/reflect/namespace_.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
bool should_use(const std::string &s) {
  if (s == "class") {
    return false;
  }
  for (const auto &i : s) {
    if (i == '{') {
      return false;
    }
    if (i == '(') {
      return false;
    }
  }
  return true;
}
//
//
//
template <typename T> std::string make_path(T b, const T &e) {
  std::string rv;

  while (b != e) {
    const auto p(*b);
    if (should_use(p)) {
      rv += "::";
      rv += p;
    }
    ++b;
  }

  return rv;
}
}
///////////////////////////////////////////////////////////////////////////////
absorb::reflect::namespace_::namespace_(const std::string &s) {
  for (auto &&s : smeeze::views::split(s, [](auto &&c) { return c == ':' || c == ' '; })) {
    _paths.emplace_back(std::string(std::begin(s), std::end(s)));
  }
}
///////////////////////////////////////////////////////////////////////////////
std::string absorb::reflect::namespace_::full() const {
  return make_path(std::begin(_paths), std::end(_paths));
}
///////////////////////////////////////////////////////////////////////////////
std::string absorb::reflect::namespace_::leading() const {
  return make_path(std::begin(_paths), std::prev(std::end(_paths)));
}
