#pragma once

// system
#include <string>
#include <vector>
// absorb
#include <absorb/visibility.h>

namespace absorb {
class API_ABSORB utils {
public:
  static std::string make_uuid();

  struct uri {
    std::string scheme;
    std::string host;
    uint16_t port;
  };
  static std::vector<uri> dissect_uri(const std::string &);

  static void hexdump(const char *, size_t);
};
}
