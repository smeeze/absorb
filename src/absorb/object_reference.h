#pragma once

// system
#include <ostream>
#include <string>

namespace absorb {
struct object_reference {
  std::string broker_uri;
  std::string object_id;

  template <typename Serializer> void serialize(Serializer &s) {
    s &broker_uri &object_id;
  }
};
}

inline std::ostream &operator<<(std::ostream &out,
                                const absorb::object_reference &o) {
  out << "absorb::object_reference(" << o.broker_uri << ", " << o.object_id
      << ")";
  return out;
}
