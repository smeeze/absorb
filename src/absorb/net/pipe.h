#pragma once

// absorb
#include <absorb/absorb.h>
#include <absorb/visibility.h>

namespace absorb {
namespace net {
class API_ABSORB pipe {
public:
  virtual ~pipe();

  size_t write_and_read(
      const absorb::buffer_view<absorb::static_buffer::const_iterator>
          &send_view,
      const absorb::buffer_view<absorb::static_buffer::iterator> &recv_view) {
    return _write_and_read(send_view, recv_view);
  }

protected:
  virtual size_t _write_and_read(
      const absorb::buffer_view<absorb::static_buffer::const_iterator> &,
      const absorb::buffer_view<absorb::static_buffer::iterator> &) = 0;
};
}
}
