#pragma once

// absorb
#include <absorb/absorb.h>
#include <absorb/object_reference.h>
#include <absorb/visibility.h>

namespace absorb {
namespace net {
class API_ABSORB dispatcher {
public:
  virtual ~dispatcher();

  void dispatch(
      const absorb::object_reference &objref,
      const absorb::buffer_view<absorb::static_buffer::const_iterator>
          &send_view,
      const absorb::buffer_view<absorb::static_buffer::iterator> &recv_view) {
    _dispatch(objref, send_view, recv_view);
  }

private:
  virtual void
  _dispatch(const absorb::object_reference &,
            const absorb::buffer_view<absorb::static_buffer::const_iterator> &,
            const absorb::buffer_view<absorb::static_buffer::iterator> &) = 0;
};
}
}
