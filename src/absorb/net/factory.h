#pragma once

// absorb
#include <absorb/absorb.h>
#include <absorb/net/pipe.h>
#include <absorb/visibility.h>

namespace absorb {
namespace net {
class factory {
public:
  virtual ~factory() {}

  std::unique_ptr<absorb::net::pipe> make_pipe(const std::string &broker_uri) {
    return _make_pipe(broker_uri);
  }

private:
  virtual std::unique_ptr<absorb::net::pipe>
  _make_pipe(const std::string &) = 0;
};
}
}
