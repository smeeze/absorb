// system
#include <cctype>
#include <regex>
// boost
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.
// fmt
#include <fmt/format.h>
// smeeze views
#include <smeeze/views/split.h>
// absorb
#include <absorb/utils.h>
///////////////////////////////////////////////////////////////////////////////
std::string absorb::utils::make_uuid() {
  boost::uuids::uuid uuid = boost::uuids::random_generator()();
  return boost::lexical_cast<std::string>(uuid);
}
///////////////////////////////////////////////////////////////////////////////
std::vector<absorb::utils::uri>
absorb::utils::dissect_uri(const std::string &u) {
  std::vector<absorb::utils::uri> rv;
  std::string regex("^([^:/]+)://([^:]+):([0-9]+)");

  for (auto &&s :
       smeeze::views::split(u, [](const char &c) { return c == ';'; })) {
    try {
      std::smatch matches;
      std::string us(std::begin(s), std::end(s));
      if (std::regex_match(us, matches, std::regex(regex))) {
        rv.push_back({matches[1], matches[2],
                      boost::lexical_cast<uint16_t>(matches[3])});
      }
    } catch (...) {
    }
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
void absorb::utils::hexdump(const char *buf, size_t size) {
  struct accum {
    ~accum() {
      while (index) {
        push(0);
      }
    }
    void push(const char c) {
      buf[index] = c;
      ++index;
      if (index == buf.size()) {
        fmt::print("{:#08x} ", count);
        for (unsigned char c : buf) {
          fmt::print(" {:02x}", c);
        }
        fmt::print("  ");
        for (const auto & c : buf) {
          if (std::isalnum(c)) {
            fmt::print("{}", c);
          } else {
            fmt::print(".");
          }
        }
        fmt::print("\n");
        count += buf.size();
        index = 0;
      }
    }
    std::array<char, 16> buf;
    size_t index = 0;
    size_t count = 0;
  };
  accum a;

  const char *buf_ptr(buf);
  size_t n(size);
  while (n--) {
    a.push(*buf_ptr);
    ++buf_ptr;
  }
}
