#pragma once

// system
#include <cstdint>
#include <type_traits>
// boost
#include <boost/type_index/ctti_type_index.hpp>
// absorb
#include <absorb/reflect/type.h>

namespace absorb {
namespace traits {
//
//
//
template <class T,
          typename std::enable_if<std::is_void<T>::value, int>::type = 0>
bool is_void() {
  return true;
}
template <class T,
          typename std::enable_if<!std::is_void<T>::value, int>::type = 0>
bool is_void() {
  return false;
}

template <class T,
          typename std::enable_if<
              std::is_const<typename std::remove_reference<T>::type>::value,
              int>::type = 0>
constexpr bool is_const() {
  return true;
}
template <class T,
          typename std::enable_if<
              !std::is_const<typename std::remove_reference<T>::type>::value,
              int>::type = 0>
constexpr bool is_const() {
  return false;
}

template <class T,
          typename std::enable_if<std::is_reference<T>::value, int>::type = 0>
bool is_ref() {
  return true;
}
template <class T,
          typename std::enable_if<!std::is_reference<T>::value, int>::type = 0>
bool is_ref() {
  return false;
}
//
//
//
struct None {};
//
//
//
template <typename... Ts> struct make_void { using type = void; };
template <typename... Ts> using void_t = typename make_void<Ts...>::type; //
//
//
//
template <class, class = void_t<>> struct is_range : std::false_type {};

template <class T>
struct is_range<T, void_t<decltype(std::declval<T>().begin()),
                          decltype(std::declval<T>().end())>> : std::true_type {
};
//
//
//
template <class, class = void_t<>>
struct has_serialize_member : std::false_type {};

template <class T>
struct has_serialize_member<
    T, void_t<decltype(std::declval<T>().serialize(std::declval<None &>()))>>
    : std::true_type {};
//
//
//
template <class, class = void_t<>>
struct has_marshal_member : std::false_type {};

template <class T>
struct has_marshal_member<
    T, void_t<decltype(std::declval<T>().marshal(std::declval<None &>()))>>
    : std::true_type {};
//
//
//
template <class, class = void_t<>>
struct has_unmarshal_member : std::false_type {};

template <class T>
struct has_unmarshal_member<
    T, void_t<decltype(std::declval<T>().unmarshal(std::declval<None &>()))>>
    : std::true_type {};
//
//
//
template <class, class = void_t<>>
struct has_resize_member : std::false_type {};

template <class T>
struct has_resize_member<
    T, void_t<decltype(std::declval<T>().resize(std::declval<std::size_t>()))>>
    : std::true_type {};
//
//
//
template <typename T> struct get_iterator_non_const_pointer {
  using type = typename std::add_pointer<
      typename std::decay<decltype(*std::declval<T>())>::type>::type;
};
//
//
//
template <typename T> struct get_iterator_const_pointer {
  using type = typename std::add_pointer<typename std::add_const<
      typename std::decay<decltype(*std::declval<T>())>::type>::type>::type;
};
//
//
//
template <typename T> struct get_container_iterator {
  using type = typename std::decay<decltype(std::declval<T>().begin())>::type;
};
//
//
//
template <size_t I, typename T, typename... Ts> struct nth_element_impl {
  using type = typename nth_element_impl<I - 1, Ts...>::type;
};

template <typename T, typename... Ts> struct nth_element_impl<0, T, Ts...> {
  using type = T;
};
//
//
//
template <size_t N> struct param_pack_helper {
  template <typename T, typename... Args> static void go(T &t) {
    param_pack_helper<N - 1>::template go<T, Args...>(t);

    using nth_type = typename nth_element_impl<N - 1, Args...>::type;

    t.emplace_back(absorb::reflect::type{
        N - 1,
        boost::typeindex::ctti_type_index::type_id<nth_type>().pretty_name(),
        is_void<nth_type>(), is_ref<nth_type>(), is_const<nth_type>()});
  }
};
//
//
//
template <> struct param_pack_helper<0> {
  template <typename T, typename... Args> static void go(T &) {}
};
//
//
//
template <typename... Args, typename Func, size_t... Is>
void visit_tuple(const std::tuple<Args...> &t, Func &&f,
                 std::index_sequence<Is...>) {
  std::initializer_list<int>{(f(std::get<Is>(t)), void(), 0)...};
}

template <typename... Args, typename Func>
void visit_tuple(const std::tuple<Args...> &t, Func &&f) {
  visit_tuple(t, f, std::index_sequence_for<Args...>{});
}
}
}
