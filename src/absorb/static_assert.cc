// absorb
#include <absorb/traits.h>
///////////////////////////////////////////////////////////////////////////////
static_assert(absorb::traits::is_const<const int>(),
              "const int must be recognized as const");
static_assert(absorb::traits::is_const<const int &>(),
              "const int & must be recognized as const");
