// system
#include <mutex>
// fmt
#include <fmt/format.h>
#include <fmt/ostream.h>
// views
#include <smeeze/views/iota.h>
#include <smeeze/views/split.h>
#include <smeeze/views/zip.h>
// self
#include <absorb/log.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  std::string get_time_of_day() {
    std::time_t result = std::time(NULL);
    std::string rv(std::ctime(&result));
    return rv.substr(0, rv.size()-1);
  }
}
///////////////////////////////////////////////////////////////////////////////
void absorb::log::print(absorb::log::level lvl, const std::string & msg) {
  static std::mutex _mtx;
  std::lock_guard<std::mutex> _{_mtx};

  for(auto z: smeeze::views::zip(
                smeeze::views::iota<int>(1024),
                smeeze::views::split(msg, [](const auto & t) { return t == '\n' || t == '\r'; })
              )
  ) {
    switch(std::get<0>(z)) {
      case 0:
        fmt::print("absorb[{}]: {}: ", lvl, get_time_of_day());
        break;
      default:
        fmt::print("                                        ", lvl, get_time_of_day());
        break;
    }
    std::string l(std::begin(std::get<1>(z)), std::end(std::get<1>(z)));
    fmt::print("{}\n", l);
  }
}
