#pragma once

// system
#include <algorithm>
#include <cstdint>
#include <type_traits>
// absorb
#include <absorb/serialize/delegate.h>
#include <absorb/traits.h>

namespace absorb {
namespace serialize {
template <typename delegate>
//
//
//
class unmarshal {
public:
  template <typename... Args>
  unmarshal(Args &&... args) : _delegate(std::forward<Args>(args)...) {}

  template <typename T> auto &operator&(T &&t) {
    _call_delegate(t);
    return *this;
  }

  void reset() { _delegate.reset(); }
  auto get_usage() const { return _delegate.get_usage(); }

private:
  template <
      class T,
      typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
  void _call_delegate(T &t) {
    _delegate.align(sizeof(t));
    _delegate.read(t);
  }

  template <class T,
            typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
  void _call_delegate(T &t) {
    typename std::underlying_type<T>::type tmp;
    _call_delegate(tmp);
    t = static_cast<T>(tmp);
  }

  template <class T,
            typename std::enable_if<absorb::traits::is_range<T>::value,
                                    int>::type = 0>
  void _call_delegate(T &t) {
    uint32_t size;
    _call_delegate(size);

    _resize(t, size);

    for (auto &v : t) {
      _call_delegate(v);
    }
  }

  template <class T,
            typename std::enable_if<
                absorb::traits::has_serialize_member<T>::value, int>::type = 0,
            typename std::enable_if<
                !absorb::traits::has_unmarshal_member<T>::value, int>::type = 0>
  void _call_delegate(T &t) {
    t.serialize(*this);
  }

  template <class T,
            typename std::enable_if<
                absorb::traits::has_unmarshal_member<T>::value, int>::type = 0>
  void _call_delegate(T &t) {
    t.unmarshal(*this);
  }

private:
  template <class T,
            typename std::enable_if<absorb::traits::has_resize_member<T>::value,
                                    int>::type = 0>
  void _resize(T &t, size_t size) {
    t.resize(size);
  }
  template <class T,
            typename std::enable_if<absorb::traits::is_range<T>::value,
                                    int>::type = 0,
            typename std::enable_if<
                !absorb::traits::has_resize_member<T>::value, int>::type = 0>
  void _resize(T &t, size_t size) {
    if ((size_t)std::distance(std::begin(t), std::end(t)) != size) {
      throw(std::runtime_error("non-resizable range has invalid size"));
    }
  }

private:
  delegate _delegate;
};
//
//
//
template <typename T> static auto make_unmarshal(T &&t) {
  using I = decltype(std::begin(t));
  return unmarshal<delegate<I>>(std::begin(t), std::end(t));
}
}
}
