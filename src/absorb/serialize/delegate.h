#pragma once

// system
#include <algorithm>
#include <stdexcept>
#include <type_traits>
// absorb
#include <absorb/absorb.h>
#include <absorb/traits.h>

namespace absorb {
namespace serialize {
//
//
//
template <typename I> class delegate {
public:
  delegate(const I &begin, const I &end)
      : _begin(begin), _iter(begin), _end(end) {}
  template <typename C>
  delegate(C &&c) : delegate(std::begin(c), std::end(c)) {}

  template <typename T> void write(const T &t) {
    auto ip(reinterpret_cast<
            typename absorb::traits::get_iterator_const_pointer<I>::type>(
        std::addressof(t)));
    const auto is(sizeof(T));

    if (is > get_remain()) {
      throw(std::runtime_error("write past buffer"));
    }

    std::copy(ip, std::next(ip, is), _iter);
    std::advance(_iter, is);
  }

  template <typename T> void read(T &t) {
    auto ip(reinterpret_cast<
            typename absorb::traits::get_iterator_non_const_pointer<I>::type>(
        std::addressof(t)));
    const auto is(sizeof(T));

    if (is > get_remain()) {
      throw(std::runtime_error("read past buffer"));
    }

    std::copy(_iter, std::next(_iter, is), ip);
    std::advance(_iter, is);
  }

  size_t get_size() const { return std::distance(_begin, _end); }
  size_t get_usage() const { return std::distance(_begin, _iter); }
  size_t get_remain() const { return std::distance(_iter, _end); }

  void align(size_t size) {
    size_t mod(get_usage() % size);
    if (mod) {
      size_t cor(size - mod);
      _iter += cor;
    }
  }

  auto get_used_view() const {
    return absorb::buffer_view<I>(I(_begin), I(_iter));
  }

  void reset() { _iter = _begin; }

private:
  const I _begin;
  I _iter;
  const I _end;
};
}
}
