#pragma once

// system
#include <algorithm>
#include <cstdint>
#include <type_traits>
// absorb
#include <absorb/serialize/delegate.h>
#include <absorb/traits.h>

namespace absorb {
namespace serialize {
template <typename delegate>
//
//
//
class marshal {
public:
  template <typename... Args>
  marshal(Args &&... args) : _delegate(std::forward<Args>(args)...) {}

  template <typename T> auto &operator&(const T &t) {
    _call_delegate(t);
    return *this;
  }

  void reset() { _delegate.reset(); }
  auto get_usage() const { return _delegate.get_usage(); }
  auto get_used_view() const { return _delegate.get_used_view(); }

private:
  template <
      class T,
      typename std::enable_if<std::is_arithmetic<T>::value, int>::type = 0>
  void _call_delegate(const T &t) {
    _delegate.align(sizeof(t));
    _delegate.write(t);
  }

  template <class T,
            typename std::enable_if<std::is_enum<T>::value, int>::type = 0>
  void _call_delegate(const T &t) {
    auto tmp(static_cast<typename std::underlying_type<T>::type>(t));
    _call_delegate(tmp);
  }

  template <class T,
            typename std::enable_if<absorb::traits::is_range<T>::value,
                                    int>::type = 0>
  void _call_delegate(const T &t) {
    auto size((uint32_t)std::distance(std::begin(t), std::end(t)));
    _call_delegate(size);

    for (const auto &v : t) {
      _call_delegate(v);
    }
  }

  template <class T,
            typename std::enable_if<
                absorb::traits::has_serialize_member<T>::value, int>::type = 0,
            typename std::enable_if<
                !absorb::traits::has_marshal_member<T>::value, int>::type = 0>
  void _call_delegate(const T &t) {
    const_cast<T &>(t).serialize(*this);
  }

  template <class T,
            typename std::enable_if<
                absorb::traits::has_marshal_member<T>::value, int>::type = 0>
  void _call_delegate(const T &t) {
    const_cast<T &>(t).marshal(*this);
  }

private:
  delegate _delegate;
};
//
//
//
template <typename T> static auto make_marshal(T &&t) {
  using I = decltype(std::begin(t));
  return marshal<delegate<I>>(std::begin(t), std::end(t));
}
}
}
