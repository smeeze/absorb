#pragma once

// system
#include <memory>
#include <unordered_map>
#include <vector>
// absorb
#include <absorb/net/dispatcher.h>
#include <absorb/net/factory.h>
#include <absorb/net/pipe.h>
#include <absorb/object_reference.h>
#include <absorb/orb/header.h>
#include <absorb/orb/reply.h>
#include <absorb/orb/skel.h>
#include <absorb/orb/stub.h>
#include <absorb/serialize/marshal.h>
#include <absorb/serialize/unmarshal.h>
#include <absorb/utils.h>

namespace absorb {
namespace orb {
template <
    typename marshal_type = absorb::serialize::marshal<
        absorb::serialize::delegate<absorb::static_buffer::iterator>>,
    typename unmarshal_type = absorb::serialize::unmarshal<
        absorb::serialize::delegate<absorb::static_buffer::const_iterator>>>
class broker : public absorb::net::pipe, public absorb::net::dispatcher {
public:
  broker() : _uuid(absorb::utils::make_uuid()) {}
  broker(const broker &) = delete;
  broker(broker &&) = delete;
  broker &operator=(const broker &) = delete;
  broker &operator=(broker &&) = delete;

  template <template <typename, typename, typename> class Serv,
            typename... Args>
  absorb::object_reference add_servant_with_id(const std::string &id,
                                               Args &&... args) {
    _servant_map.insert(std::make_pair(
        id, std::move(std::make_unique<
                      Serv<absorb::orb::skel<marshal_type, unmarshal_type>,
                           marshal_type, unmarshal_type>>(
                std::forward<Args>(args)...))));
    return {_uuid, id};
  }

  template <template <typename, typename, typename> class Serv,
            typename... Args>
  absorb::object_reference add_servant(Args &&... args) {
    return add_servant_with_id<Serv>(absorb::utils::make_uuid(),
                                     std::forward<Args>(args)...);
  }

  template <template <typename> class T>
  auto make_stub(const absorb::object_reference &o) {
    return T<absorb::orb::stub<marshal_type, unmarshal_type>>(o, this);
  }

  void add_factory(absorb::net::factory *f) { _net_factories.push_back(f); }

  // orb override
  std::string get_uuid() const { return _uuid; }

private:
  absorb::net::pipe *_locate_pipe(const std::string &broker_uri) {
    if (broker_uri == _uuid) {
      return this;
    }
    if (_pipe_map.find(broker_uri) == _pipe_map.end()) {
      _make_pipe(broker_uri);
    }
    return _pipe_map.at(broker_uri).get();
  }

  void _make_pipe(const std::string &broker_uri) {
    for (auto *f : _net_factories) {
      try {
        _pipe_map.insert(std::make_pair(broker_uri, f->make_pipe(broker_uri)));
        return;
      } catch (...) {
      }
      throw(std::runtime_error("could not make pipe"));
    }
  }
  std::vector<absorb::net::factory *> _net_factories;

private:
  // dispatcher override
  void _dispatch(
      const absorb::object_reference &objref,
      const absorb::buffer_view<absorb::static_buffer::const_iterator> &sv,
      const absorb::buffer_view<absorb::static_buffer::iterator> &rv) override {
    _locate_pipe(objref.broker_uri)->write_and_read(sv, rv);
  }

  std::unordered_map<std::string, std::unique_ptr<absorb::net::pipe>> _pipe_map;

private:
  // pipe override
  size_t _write_and_read(
      const absorb::buffer_view<absorb::static_buffer::const_iterator> &rv,
      const absorb::buffer_view<absorb::static_buffer::iterator> &sv) override {
    marshal_type m(sv);
    unmarshal_type u(rv);

    absorb::orb::header hdr;
    std::string object_id;
    std::string function_name;

    // header of try/catch, because server needs to close pipe on error
    u &hdr;
    try {
      u &object_id;
      u &function_name;

      auto s(_servant_map.find(object_id));
      if (s == _servant_map.end()) {
        throw(std::runtime_error(std::string("could not find object id: ") +
                                 object_id));
      }

      m &hdr;
      m &absorb::orb::reply();
      s->second->call(function_name, m, u);
    } catch (const std::exception &e) {
      m.reset();
      m &hdr;
      m &absorb::orb::reply(e.what());
    }

    return m.get_usage();
  }

private:
  std::string _uuid;
  std::unordered_map<
      std::string,
      std::unique_ptr<absorb::orb::skel<marshal_type, unmarshal_type>>>
      _servant_map;
};
}
}
