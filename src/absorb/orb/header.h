#pragma once

// system
#include <array>
#include <stdexcept>

namespace absorb {
namespace orb {
  class header {
  public:
    template<typename Serializer>
    void marshal(Serializer & s) {
	  for (const auto & c : _magic) {
	    s & c;
      }
    }

    template<typename Serializer>
    void unmarshal(Serializer & s) {
      magic tmp;
	  for (auto & c : tmp) {
	    s & c;
      }

      if(!std::equal(std::begin(tmp), std::end(tmp), std::begin(_magic))) {
        throw(std::runtime_error("invalid magic header"));
      }
    }
  private:
    using magic = std::array<char, 6>;
    const magic _magic = {{'a','b','s','o','r','b'}};
  };
}
}
