#pragma once

// system
#include <string>

namespace absorb {
namespace orb {
template <typename marshal_type, typename unmarshal_type>
class skel {
public:
  virtual ~skel() { }

  void call(const std::string &f, marshal_type &m, unmarshal_type &u) {
    _call(f, m, u);
  }

protected:
  virtual void _call(const std::string &, marshal_type &, unmarshal_type &) = 0;
};
}
}
