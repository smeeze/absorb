#pragma once

// system
#include <array>
// absorb
#include <absorb/absorb.h>
#include <absorb/net/dispatcher.h>
#include <absorb/object_reference.h>
#include <absorb/orb/header.h>
#include <absorb/orb/reply.h>

namespace absorb {
namespace orb {
template <typename marshal_type, typename unmarshal_type> class stub {
public:
  stub(const absorb::object_reference &o, absorb::net::dispatcher *d)
      : _object_reference(o), _dispatcher(d) {}

protected:
  template <typename T1, typename T2>
  void _call_function(const std::string &function_name, T1 t1, T2 t2) const {
    marshal_type m(_send_buffer);
    unmarshal_type u(_recv_buffer);

    absorb::orb::reply rpl;

    m &absorb::orb::header();
    m &_object_reference.object_id;
    m &function_name;
    m &t1;

    _dispatcher->dispatch(_object_reference, m.get_used_view(), _recv_buffer);

    u &absorb::orb::header();
    u &rpl;
    u &t2;
  }

private:
  absorb::object_reference _object_reference;
  mutable absorb::net::dispatcher *_dispatcher;
  mutable absorb::static_buffer _send_buffer;
  mutable absorb::static_buffer _recv_buffer;
};
}
}
