#pragma once

// system
#include <cassert>
#include <string>

namespace absorb {
namespace orb {
class reply {
public:
  reply() : _status(0) {}
  reply(const std::string &msg) : _status(-1), _msg(msg) {
    assert(_status != 0);
  }

  template <typename Serializer> void marshal(Serializer &s) {
    s &_status;

    if (_status) {
      s &_msg;
    }
  }

  template <typename Serializer> void unmarshal(Serializer &s) {
    s &_status;
    if (_status) {
      s &_msg;
      throw(std::runtime_error(_msg));
    }
  }

private:
  int8_t _status;
  std::string _msg;
};
}
}
