#pragma once

// system
#include <ostream>
#include <string>
// fmt
#include <fmt/ostream.h>
// absorb
#include <absorb/visibility.h>

namespace absorb {
class API_ABSORB log {
public:
  enum class level { debug, error, warning, info };

  static void print(level, const std::string &);

  template <typename... Args> static void format(level lvl, Args &&... args) {
    print(lvl, fmt::format(std::forward<Args>(args)...));
  }
};
}

inline std::ostream &operator<<(std::ostream &out,
                                const absorb::log::level &lvl) {
  switch (lvl) {
  case absorb::log::level::debug:
    out << "dbg";
    break;
  case absorb::log::level::error:
    out << "err";
    break;
  case absorb::log::level::warning:
    out << "wrn";
    break;
  case absorb::log::level::info:
    out << "inf";
    break;
  }
  return out;
}
