#pragma once

// system
#include <chrono>
#include <thread>
#include <vector>
// absorb
#include <absorb/modules/asio/tcp/pipe.h>
#include <absorb/modules/asio/tcp/server.h>
#include <absorb/net/factory.h>
#include <absorb/utils.h>

namespace absorb {
namespace modules {
namespace asio {
class runner : public absorb::net::factory {
public:
  runner(uint32_t thread_count) { _start(thread_count); }
  runner(uint32_t thread_count, uint16_t port, absorb::net::pipe *handler)
      : _server(std::make_unique<absorb::modules::asio::tcp::server>(
            _io_service, port, handler)) {
    _start(thread_count);
  }

  ~runner() { _stop(); }

  std::string resolve_host(const std::string &hostname) {
    boost::asio::ip::tcp::resolver resolver(_io_service);
    boost::asio::ip::tcp::resolver::query query(hostname, "");

    boost::asio::ip::tcp::resolver::iterator destination =
        resolver.resolve(query);
    boost::asio::ip::tcp::resolver::iterator end;
    boost::asio::ip::tcp::endpoint endpoint;

    while (destination != end) {
      endpoint = *destination++;
      if (endpoint.address().is_v4()) {
        const auto rv(endpoint.address().to_string());
        return rv;
      }
    }
    throw(std::runtime_error("could not resolve hostname"));
  }

private:
  void _start(uint32_t thread_count) {
    _running = true;
    _thread_vector.reserve(thread_count);
    for (size_t n = 0; n < thread_count; ++n) {
      _thread_vector.emplace_back([&]() {
        while (_running) {
          _io_service.run();
          std::this_thread::sleep_for(std::chrono::milliseconds(1000 / 10));
        }
      });
    }
  }
  void _stop() {
    _running = false;
    _io_service.stop();
    for (auto &t : _thread_vector) {
      t.join();
    }
  }

private:
  boost::asio::io_service _io_service;
  std::unique_ptr<absorb::modules::asio::tcp::server> _server;

  std::vector<std::thread> _thread_vector;
  bool _running;

protected:
  // factory override
  std::unique_ptr<absorb::net::pipe>
  _make_pipe(const std::string &broker_uri) override {
    const auto uriv(absorb::utils::dissect_uri(broker_uri));
    for (const auto &u : uriv) {
      if (u.scheme == "tcp") {
        return std::make_unique<absorb::modules::asio::tcp::pipe>(
            _io_service,
            boost::asio::ip::address::from_string(resolve_host(u.host)),
            u.port);
      }
    }
    throw(std::runtime_error("could not make pipe"));
  }
};
}
}
}
