#pragma once

// system
#include <array>
#include <stdexcept>
// boost
#include <boost/asio.hpp>
// absorb
#include <absorb/net/pipe.h>
#include <absorb/visibility.h>

namespace absorb {
namespace modules {
namespace asio {
namespace tcp {
class API_ABSORB pipe : public absorb::net::pipe {
public:
  pipe(boost::asio::io_service &, const boost::asio::ip::tcp::endpoint &);
  pipe(boost::asio::io_service &, const boost::asio::ip::address &, short);

protected:
  size_t _write_and_read(
      const absorb::buffer_view<absorb::static_buffer::const_iterator> &,
      const absorb::buffer_view<absorb::static_buffer::iterator> &) override;

private:
  boost::asio::ip::tcp::socket _socket;
};
}
}
}
}
