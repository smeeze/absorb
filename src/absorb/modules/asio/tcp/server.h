#pragma once

// system
#include <array>
#include <functional>
#include <memory>
#include <tuple>
// boost
#include <boost/asio.hpp>
// absorb
#include <absorb/log.h>
#include <absorb/net/pipe.h>
#include <absorb/utils.h>

namespace absorb {
namespace modules {
namespace asio {
namespace tcp {
class server {
  class session : public std::enable_shared_from_this<session> {
  public:
    session(boost::asio::ip::tcp::socket socket, absorb::net::pipe *p)
        : _socket(std::move(socket)), _pipe(p) {
      absorb::log::format(absorb::log::level::info, "session: {}",
                          _socket.remote_endpoint().address().to_string());
    }
    ~session() {
      absorb::log::format(absorb::log::level::info,
                          "~session: {} read: {}x {}B written: {}x {}B",
                          _socket.remote_endpoint().address().to_string(),
                          _read_counter, _read_bytes, _write_counter,
                          _write_bytes);
    }

    void start() { do_read(); }

  private:
    void do_read() {
      auto self(this->shared_from_this());
      _socket.async_read_some(
          boost::asio::buffer(_recv_buffer.data(), _recv_buffer.size()),
          [this, self](boost::system::error_code ec, std::size_t length) {
            if (!ec) {
              try {
                ++_read_counter;
                _read_bytes += length;
                const auto s(_pipe->write_and_read(
                    absorb::buffer_view<absorb::static_buffer::const_iterator>(
                        std::begin(_recv_buffer),
                        std::next(std::begin(_recv_buffer), length)),
                    _send_buffer));
                do_write(s);
              } catch (...) {
                absorb::utils::hexdump(
                    // static_cast<const unsigned char *>(_recv_buffer.data()),
                    _recv_buffer.data(), length);
                return;
              }
            } else if (ec == boost::asio::error::eof) {
              return;
            }
          });
    }

    void do_write(std::size_t length) {
      auto self(this->shared_from_this());
      boost::asio::async_write(
          _socket, boost::asio::buffer(_send_buffer.data(), length),
          [this, self](boost::system::error_code ec, std::size_t length) {
            ++_write_counter;
            _write_bytes += length;
            if (!ec) {
              do_read();
            }
          });
    }

    boost::asio::ip::tcp::socket _socket;
    absorb::net::pipe *_pipe;
    absorb::static_buffer _recv_buffer;
    absorb::static_buffer _send_buffer;
    uint64_t _read_bytes = 0;
    uint64_t _write_bytes = 0;
    uint64_t _read_counter = 0;
    uint64_t _write_counter = 0;
  };

public:
  server(boost::asio::io_service &io_service, short port, absorb::net::pipe *p)
      : _acceptor(io_service, boost::asio::ip::tcp::endpoint(
                                  boost::asio::ip::tcp::v4(), port)),
        _socket(io_service), _pipe(p) {
    _do_accept();
  }
  auto get_address() const { return _acceptor.local_endpoint().address(); }
  auto get_port() const { return _acceptor.local_endpoint().port(); }

private:
  void _do_accept() {
    _acceptor.async_accept(_socket, [this](boost::system::error_code ec) {
      if (!ec) {
        std::make_shared<session>(std::move(_socket), _pipe)->start();
      }

      _do_accept();
    });
  }

  boost::asio::ip::tcp::acceptor _acceptor;
  boost::asio::ip::tcp::socket _socket;
  absorb::net::pipe *_pipe;
};
}
}
}
}
