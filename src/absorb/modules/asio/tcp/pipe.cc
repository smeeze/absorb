// absorb
#include <absorb/modules/asio/tcp/pipe.h>
///////////////////////////////////////////////////////////////////////////////
absorb::modules::asio::tcp::pipe::pipe(
    boost::asio::io_service &io_service,
    const boost::asio::ip::tcp::endpoint &endpoint)
    : _socket(io_service) {
  _socket.connect(endpoint);
  if (!_socket.is_open()) {
    throw(std::runtime_error("asio, tcp socket not open"));
  }
}
///////////////////////////////////////////////////////////////////////////////
absorb::modules::asio::tcp::pipe::pipe(boost::asio::io_service &io_service,
                                       const boost::asio::ip::address &address,
                                       short port)
    : pipe(io_service, boost::asio::ip::tcp::endpoint(address, port)) {}
///////////////////////////////////////////////////////////////////////////////
size_t absorb::modules::asio::tcp::pipe::_write_and_read(
    const absorb::buffer_view<absorb::static_buffer::const_iterator> &recv_view,
    const absorb::buffer_view<absorb::static_buffer::iterator> &send_view) {

  _socket.write_some(
      boost::asio::buffer(&*recv_view.begin(), recv_view.size()));
  return _socket.read_some(
      boost::asio::buffer(&*send_view.begin(), send_view.size()));
}
