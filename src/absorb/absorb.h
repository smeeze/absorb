#pragma once

// system
#include <array>

namespace absorb {
using static_buffer = std::array<char, 1024>;

template <typename I> class buffer_view {
public:
  buffer_view(const I &b, const I &e) : _begin(b), _end(e) {}

  template <typename C>
  buffer_view(C &&c) : buffer_view(std::begin(c), std::end(c)) {}

  auto size() const { return std::distance(begin(), end()); }

  const I &begin() const { return _begin; }
  const I &end() const { return _end; }

  I &begin() { return _begin; }
  I &end() { return _end; }

private:
  I _begin;
  I _end;
};
}
