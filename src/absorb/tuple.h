#pragma once

// system
#include <ostream>
#include <tuple>
// absorb
#include <absorb/traits.h>

namespace absorb {
  //
  //
  //
  template<typename... Items>
  class tuple {
  public:
    tuple(Items && ... items) : _items(std::forward<Items>(items)...) { }

    template<typename Serializer>
    void serialize(Serializer & s) {
      absorb::traits::visit_tuple(
        _items,
        [&s](auto & e) {
          s & e;
        }
      );
    }
  private:
    std::tuple<Items...> _items;
  };
  //
  //
  //
  template<typename... Items>
  auto
  make_tuple(Items && ... items) {
    return tuple<Items...>(std::forward<Items>(items)...);
  }
}
