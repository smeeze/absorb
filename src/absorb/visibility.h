#pragma once

#ifdef WIN32
  #define API_EXPORT_BASE __declspec(dllexport)
#else
  #define API_EXPORT_BASE __attribute__ ((visibility("default")))
#endif

#ifdef DO_EXPORT_ABSORB
  #define API_ABSORB API_EXPORT_BASE
#else
  #define API_ABSORB
#endif
