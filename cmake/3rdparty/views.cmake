include(FetchContent)
FetchContent_Declare(
  views
  GIT_REPOSITORY https://gitlab.com/smeeze/views.git
)

FetchContent_GetProperties(views)
if(NOT views_POPULATED)
  FetchContent_Populate(views)
  set(VIEWS_INCLUDE_DIR ${views_SOURCE_DIR}/include PARENT_SCOPE)
endif()
