include(FetchContent)
FetchContent_Declare(
  nanomsg
  GIT_REPOSITORY https://github.com/nanomsg/nanomsg
  GIT_TAG 1.1.3
  GIT_SHALLOW TRUE
)

FetchContent_GetProperties(nanomsg)
if(NOT nanomsg_POPULATED)
  FetchContent_Populate(nanomsg)
  set(NANOMSG_INCLUDE_DIR ${nanomsg_SOURCE_DIR}/include PARENT_SCOPE)
  add_subdirectory(${nanomsg_SOURCE_DIR} ${nanomsg_BINARY_DIR} EXCLUDE_FROM_ALL)
endif()
