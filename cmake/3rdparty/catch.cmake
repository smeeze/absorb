include(FetchContent)
FetchContent_Declare(
  catch
  GIT_REPOSITORY https://github.com/philsquared/Catch.git
  GIT_TAG v2.0.1
  GIT_SHALLOW TRUE
)

FetchContent_GetProperties(catch)
if(NOT catch_POPULATED)
  FetchContent_Populate(catch)
  set(CATCH_INCLUDE_DIR ${catch_SOURCE_DIR}/single_include PARENT_SCOPE)
endif()
