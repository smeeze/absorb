if(NOT EXISTS ${BOOST_SOURCES}/boost/config.hpp)
  message(FATAL_ERROR "BOOST_SOURCES(${BOOST_SOURCES}) must point to valid boost source tree")
endif()

################
# boost system #
################
add_library(boost-system SHARED
  ${BOOST_SOURCES}/libs/system/src/error_code.cpp
)
target_include_directories(boost-system SYSTEM PRIVATE ${BOOST_SOURCES})
target_compile_definitions(boost-system        PRIVATE BOOST_ALL_NO_LIB)
target_compile_definitions(boost-system        PRIVATE BOOST_ALL_DYN_LINK)
install(TARGETS boost-system DESTINATION lib)

#########################
# boost program options #
#########################
add_library(boost-program_options STATIC
  ${BOOST_SOURCES}/libs/program_options/src/cmdline.cpp
  ${BOOST_SOURCES}/libs/program_options/src/config_file.cpp
  ${BOOST_SOURCES}/libs/program_options/src/convert.cpp
  ${BOOST_SOURCES}/libs/program_options/src/options_description.cpp
  ${BOOST_SOURCES}/libs/program_options/src/parsers.cpp
  ${BOOST_SOURCES}/libs/program_options/src/positional_options.cpp
  ${BOOST_SOURCES}/libs/program_options/src/split.cpp
  ${BOOST_SOURCES}/libs/program_options/src/utf8_codecvt_facet.cpp
  ${BOOST_SOURCES}/libs/program_options/src/value_semantic.cpp
  ${BOOST_SOURCES}/libs/program_options/src/variables_map.cpp
  ${BOOST_SOURCES}/libs/program_options/src/winmain.cpp
)
target_include_directories(boost-program_options SYSTEM PRIVATE ${BOOST_SOURCES})
target_compile_definitions(boost-program_options        PRIVATE BOOST_ALL_NO_LIB)
target_compile_definitions(boost-program_options        PRIVATE BOOST_ALL_DYN_LINK)
