// system
#include <array>
// catch
#include <catch.hpp>
// absorb
#include <absorb/absorb.h>
#include <absorb/serialize/marshal.h>
#include <absorb/serialize/unmarshal.h>
#include <absorb/tuple.h>
// self
#include <test/coord.h>
#include <test/person.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
template <typename T> void serialize(const T &in, T &out) {
  absorb::static_buffer buffer;

  {
    auto m(absorb::serialize::make_marshal(buffer));
    m &in;
  }
  {
    auto um(absorb::serialize::make_unmarshal(buffer));
    um &out;
  }
}
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-int") {
  int in(13);
  while (in < 1e6) {
    int out(0);
    serialize(in, out);
    REQUIRE(in == out);
    in *= 13;
  }
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-string") {
  std::string in("kwyjibo");
  std::string out;
  serialize(in, out);
  REQUIRE(in == out);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-string-std-vector") {
  std::string in("kwyjibo");
  std::string out;

  std::vector<uint8_t> buffer(1024);
  auto m(absorb::serialize::make_marshal(buffer));
  m &in;

  auto um(absorb::serialize::make_unmarshal(buffer));
  um &out;

  REQUIRE(in == out);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-string-char-pointer") {
  std::string in("kwyjibo");
  std::string out;

  absorb::static_buffer buffer;
  absorb::serialize::marshal<absorb::serialize::delegate<char *>> m(
      buffer.data(), buffer.data() + buffer.size());
  m &in;

  absorb::serialize::unmarshal<absorb::serialize::delegate<const char *>> um(
      buffer.data(), buffer.data() + buffer.size());
  um &out;

  REQUIRE(in == out);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-vector-string") {
  std::vector<std::string> in{
      "homer", "marge", "bart", "lisa", "maggie",
  };
  std::vector<std::string> out;
  serialize(in, out);
  REQUIRE(in.size() == out.size());
  REQUIRE(
      std::equal(std::begin(in), std::end(in), std::begin(out), std::end(out)));
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-person") {
  person in{10, "bart"};
  person out;
  serialize(in, out);
  REQUIRE(in == out);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-vector-person") {
  std::vector<person> in{
      {36, "homer"}, {34, "marge"}, {10, "bart"}, {8, "lisa"}, {1, "maggie"},
  };
  std::vector<person> out;
  serialize(in, out);
  REQUIRE(in.size() == out.size());
  REQUIRE(
      std::equal(std::begin(in), std::end(in), std::begin(out), std::end(out)));
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-multi") {
  int in0(13);
  int in1(14);
  int in2(15);
  int out0(0);
  int out1(0);
  int out2(0);
  absorb::static_buffer buffer;

  auto m(absorb::serialize::make_marshal(buffer));
  m &in0 &in1 &in2;

  auto um(absorb::serialize::make_unmarshal(buffer));
  um &out0 &out1 &out2;

  REQUIRE(in0 == out0);
  REQUIRE(in1 == out1);
  REQUIRE(in2 == out2);
}
///////////////////////////////////////////////////////////////////////////////
TEST_CASE("absorb-serialize-tuple") {
  int in0(13);
  int in1(14);
  int in2(15);
  int out0(0);
  int out1(0);
  int out2(0);
  auto in(absorb::make_tuple(in0, in1, in2));
  auto out(absorb::make_tuple(out0, out1, out2));
  serialize(in, out);
  REQUIRE(in0 == out0);
  REQUIRE(in1 == out1);
  REQUIRE(in2 == out2);
}
