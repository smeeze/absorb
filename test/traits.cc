// absorb
#include <absorb/log.h>
#include <absorb/traits.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
class test_const {
public:
  void func1() const {}
};
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
#if 0
  std::cout << "test_const::func1, is void: "
            << absorb::traits::is_const<test_const::func1>() << std::endl;
#endif
  return 0;
}
