// absorb
#include <absorb/absorb.h>
#include <absorb/log.h>
#include <absorb/modules/asio/runner.h>
#include <absorb/modules/asio/tcp/pipe.h>
#include <absorb/modules/asio/tcp/server.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
class handler : public absorb::net::pipe {
protected:
  size_t _write_and_read(
      const absorb::buffer_view<absorb::static_buffer::const_iterator>
          &send_view,
      const absorb::buffer_view<absorb::static_buffer::iterator> &recv_view)
      override {
    std::copy(std::begin(send_view), std::end(send_view),
              std::begin(recv_view));
    return std::distance(std::begin(send_view), std::end(send_view));
  }
};
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
  handler h;

  absorb::modules::asio::runner runner(4, 10001, &h);

  auto pipe(runner.make_pipe("tcp://127.0.0.1:10001"));

  absorb::static_buffer b1;
  absorb::static_buffer b2;

  std::string msg1("this is a message");
  std::string msg2;

  std::copy(std::begin(msg1), std::end(msg1), std::begin(b1));
  const auto nbytes(pipe->write_and_read(
      absorb::buffer_view<absorb::static_buffer::const_iterator>(
          std::begin(b1), std::next(std::begin(b1), msg1.size())),
      absorb::buffer_view<absorb::static_buffer::iterator>(b2)));

  msg2.resize(1024);
  std::copy(std::begin(b2), std::next(std::begin(b2), nbytes),
            std::begin(msg2));

  absorb::log::format(absorb::log::level::info, "msg1: {}", msg1);
  absorb::log::format(absorb::log::level::info, "nbytes: {}", nbytes);
  absorb::log::format(absorb::log::level::info, "msg2: {}", msg2);

  return 0;
}
