#pragma once

struct coord {
  float x;
  float y;

  bool operator==(const coord & c) const { return x == c.x && y == c.y; }
};

namespace absorb {
namespace non_intrusive {
  template<typename S>
  S & serialize(S & s, coord & c) {
    s & c.x & c.y;
    return s;
  }
}
}
