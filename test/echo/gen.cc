// absorb
#include <absorb/log.h>
#include <absorb/reflect/class_.h>
#include <absorb/reflect/generate.h>
// test
#include <echo/echo.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    auto ms(absorb::reflect::class_<test::echo::echo>()
                .def("talk", &test::echo::echo::talk)
                .def("yell", &test::echo::echo::yell)
                .def("throw_error", &test::echo::echo::throw_error));

    absorb::reflect::generate::stub("echo_stub.h", ms);
    absorb::reflect::generate::skeleton("echo_skel.h", ms);
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }

  return 0;
}
