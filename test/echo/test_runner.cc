// system
#include <chrono>
// absorb
#include <absorb/log.h>
// test
#include <echo/test_runner.h>
///////////////////////////////////////////////////////////////////////////////
void test::echo::test_runner::talk_yell(test::echo::echo &&stub) {
  absorb::log::format(absorb::log::level::info, "----- {} -----", __FUNCTION__);
  absorb::log::format(absorb::log::level::info, "talk: {}", stub.talk("hello"));
  absorb::log::format(absorb::log::level::info, "yell: {}", stub.yell("hello"));
}
///////////////////////////////////////////////////////////////////////////////
void test::echo::test_runner::except(test::echo::echo &&stub) {
  absorb::log::format(absorb::log::level::info, "----- {} -----", __FUNCTION__);
  try {
    stub.throw_error("client message");
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "exception: {}", e.what());
  }
}
///////////////////////////////////////////////////////////////////////////////
void test::echo::test_runner::speed(test::echo::echo &&stub) {
  absorb::log::format(absorb::log::level::info, "----- {} -----", __FUNCTION__);
  const auto stamp(std::chrono::high_resolution_clock::now() +
                   std::chrono::seconds(1));
  uint32_t n(0);
  while (std::chrono::high_resolution_clock::now() < stamp) {
    stub.talk("hello");
    stub.yell("hello");
    ++n;
  }
  absorb::log::format(absorb::log::level::info, "after 1 second, n={}", n);
}
