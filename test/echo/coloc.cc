// system
#include <memory>
// absorb
#include <absorb/log.h>
#include <absorb/orb/broker.h>
#include <absorb/orb/skel.h>
// test
#include <echo/echo.h>
#include <echo/echo_servant.h>
#include <echo/test_runner.h>
// gen
#include <echo_stub.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
  try {
    // broker
    absorb::orb::broker<> broker;

    // servant
    const auto o(broker.add_servant<test::echo::echo_servant>());
    const absorb::object_reference n{"", ""};

    // stub
    test::echo::test_runner::talk_yell(broker.make_stub<echo_stub>(o));
    test::echo::test_runner::except(broker.make_stub<echo_stub>(o));
    test::echo::test_runner::except(broker.make_stub<echo_stub>(n));
    test::echo::test_runner::speed(broker.make_stub<echo_stub>(o));
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }
  return 0;
}
