#pragma once

// test
#include <echo/echo.h>

namespace test {
namespace echo {
class test_runner {
public:
  static void talk_yell(test::echo::echo &&);
  static void except(test::echo::echo &&);
  static void speed(test::echo::echo &&);
};
}
}
