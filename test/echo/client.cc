// system
#include <chrono>
// absorb
#include <absorb/log.h>
#include <absorb/modules/asio/runner.h>
#include <absorb/orb/broker.h>
// test
#include <echo/echo.h>
#include <echo/test_runner.h>
// gen
#include <echo_stub.h>
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
  try {
    // broker
    absorb::orb::broker<> broker;

    // asio
    absorb::modules::asio::runner asio_runner(4);
    broker.add_factory(&asio_runner);

    std::string broker_id("tcp://127.0.0.1:10001");
    if (argc >= 2) {
      broker_id = argv[1];
    }
    absorb::log::format(absorb::log::level::info, "using broker id: {}",
                        broker_id);

    const absorb::object_reference o{broker_id, "echo"};
    const absorb::object_reference n{"", ""};

    test::echo::test_runner::talk_yell(broker.make_stub<echo_stub>(o));
    test::echo::test_runner::except(broker.make_stub<echo_stub>(o));
    test::echo::test_runner::except(broker.make_stub<echo_stub>(n));
    test::echo::test_runner::speed(broker.make_stub<echo_stub>(o));
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }
  return 0;
}
