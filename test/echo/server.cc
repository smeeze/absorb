// system
#include <cstdio>
// absorb
#include <absorb/log.h>
#include <absorb/modules/asio/runner.h>
#include <absorb/orb/broker.h>
// test
#include <echo/echo_servant.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
  try {
    // broker
    absorb::orb::broker<> broker;

    // asio
    absorb::modules::asio::runner asio_runner(4, 10001, &broker);

    absorb::log::format(absorb::log::level::info, "server started, uuid: {}",
                        broker.get_uuid());

    // servant
    const auto o(broker.add_servant_with_id<test::echo::echo_servant>("echo"));

    absorb::log::format(absorb::log::level::info, "press enter to exit");
    getchar();
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }
  return 0;
}
