#pragma once

// test
#include <echo/echo.h>
// gen
#include <echo_skel.h>

namespace test {
namespace echo {
template <typename B, typename U, typename M>
class echo_servant : public absorb::gen::echo_skeleton<B, U, M> {
public:
  std::string talk(const std::string &msg) const override { return msg; }
  std::string yell(const std::string &msg) const override {
    std::string rv(msg);
    for (auto &s : rv) {
      s = toupper(s);
    }
    return rv;
  }
  void throw_error(const std::string &msg) const override {
    throw(std::runtime_error(std::string("servant throws: ") + msg));
  }
};
}
}
