#pragma once

// system
#include <string>

namespace test {
namespace echo {
class echo {
public:
  virtual ~echo();

  virtual std::string talk(const std::string &) const = 0;
  virtual std::string yell(const std::string &) const = 0;
  virtual void throw_error(const std::string &) const = 0;
};
}
}
