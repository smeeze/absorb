#pragma once

// system
#include <string>

struct person {
  int         age;
  std::string name;

  template<typename Serializer>
  void serialize(Serializer & s) {
    s & age & name;
  }

  bool operator==(const person & p) const { return age == p.age && name == p.name; }
};
