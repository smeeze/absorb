#pragma once

// test
#include <calc/calc.h>
// gen
#include <calc_skel.h>

namespace test {
namespace calc {
template <typename B, typename U, typename M>
class calc_servant : public absorb::gen::calc_skeleton<B, U, M> {
public:
  calc_servant(int n) : _n(n) {}

  int sum(int a, int b) const override { return a + b; }
  int sum(int a, int b, int c) const override { return a + b + c; }
  void sum_inplace(int &result, int a, int b) const override { result = a + b; }
  void increment(int &a, int &b) const override {
    a += _n;
    b += _n;
  }

private:
  int _n;
};
}
}
