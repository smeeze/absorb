// absorb
#include <absorb/log.h>
// test
#include <calc/test_runner.h>
///////////////////////////////////////////////////////////////////////////////
void test::calc::test_runner::test1(test::calc::calc &&stub) {
  absorb::log::format(absorb::log::level::info, "----- {} -----", __FUNCTION__);
  int r(0);
  int a(13);
  int b(14);
  int c(15);
  absorb::log::format(absorb::log::level::info, "sum({} + {}): {}", a, b,
                      stub.sum(a, b));
  absorb::log::format(absorb::log::level::info, "sum({} + {} + {}): {}", a, b,
                      c, stub.sum(a, b, c));

  stub.sum_inplace(r, a, b);
  absorb::log::format(absorb::log::level::info, "sum_inplace({}  {}): ", a, b,
                      r);

  stub.increment(a, b);
  absorb::log::format(absorb::log::level::info, "increment: {}, {}", a, b);
}
///////////////////////////////////////////////////////////////////////////////
void test::calc::test_runner::test2(test::calc::calc &&stub) {
  absorb::log::format(absorb::log::level::info, "----- {} -----", __FUNCTION__);
  int a(13);
  int b(14);
  stub.increment(a, b);
  absorb::log::format(absorb::log::level::info, "increment: {}, {}", a, b);
}
