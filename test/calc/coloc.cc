// system
#include <memory>
// absorb
#include <absorb/log.h>
#include <absorb/orb/broker.h>
#include <absorb/orb/skel.h>
// test
#include <calc/calc_servant.h>
#include <calc/test_runner.h>
// gen
#include <calc_stub.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **argv) {
  absorb::log::format(absorb::log::level::info, "--- {} ---", argv[0]);
  try {
    // broker
    absorb::orb::broker<> broker;

    // servant
    const auto o10(broker.add_servant<test::calc::calc_servant>(10));
    const auto o20(broker.add_servant<test::calc::calc_servant>(20));

    // stub
    test::calc::test_runner::test1(broker.make_stub<calc_stub>(o10));
    test::calc::test_runner::test2(broker.make_stub<calc_stub>(o20));
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }
  return 0;
}
