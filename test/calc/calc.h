#pragma once

namespace test {
namespace calc {
class calc {
public:
  virtual ~calc();

  virtual int sum(int, int) const = 0;
  virtual int sum(int, int, int) const = 0;
  virtual void sum_inplace(int &, int, int) const = 0;
  virtual void increment(int &, int &) const = 0;
};
}
}
