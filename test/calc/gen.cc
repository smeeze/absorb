// absorb
#include <absorb/log.h>
#include <absorb/reflect/class_.h>
#include <absorb/reflect/generate.h>
// test
#include <calc/calc.h>
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    auto ms(absorb::reflect::class_<test::calc::calc>()
                .def("sum",
                     (int (test::calc::calc::*)(int, int) const) &
                         test::calc::calc::sum)
                .def("sum",
                     (int (test::calc::calc::*)(int, int, int) const) &
                         test::calc::calc::sum)
                .def("sum_inplace", &test::calc::calc::sum_inplace)
                .def("increment", &test::calc::calc::increment));

    absorb::reflect::generate::stub("calc_stub.h", ms);
    absorb::reflect::generate::skeleton("calc_skel.h", ms);
  } catch (const std::exception &e) {
    absorb::log::format(absorb::log::level::error, "error: {}", e.what());
    return -1;
  }
  return 0;
}
